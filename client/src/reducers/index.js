import { combineReducers } from 'redux';
import GroupsReducer from './reducer_groups';
import UsersReducer from './reducer_users';
import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers({
  groups: GroupsReducer,
  users: UsersReducer,
  form: formReducer
});

export default rootReducer;
