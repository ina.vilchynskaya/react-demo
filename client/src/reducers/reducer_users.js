import { FETCH_USERS, FETCH_USER, DELETE_USERS } from '../actions';

const INITIAL_STATE = { all: [], group: null };

export default function(state = INITIAL_STATE, action){
  switch(action.type){
  case DELETE_USERS:
    return _.reject(state, user => user._id === action.payload);
  case FETCH_USERS:
    return { ...state, all: action.payload.data }
  case FETCH_USER:
    return { ...state, user: action.payload.data }
  default:
    return state;
  }
}
