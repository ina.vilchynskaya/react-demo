import { FETCH_GROUPS, FETCH_GROUP, DELETE_GROUP } from '../actions';

const INITIAL_STATE = { all: [], group: null };

export default function(state = INITIAL_STATE, action){
  switch(action.type){
  case DELETE_GROUP:
    return _.reject(state, group => group._id === action.payload);
  case FETCH_GROUPS:
    return { ...state, all: action.payload.data }
  case FETCH_GROUP:
    return { ...state, group: action.payload.data }
  default:
    return state;
  }
}
