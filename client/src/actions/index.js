import axios from 'axios';
import _ from 'lodash';

export const FETCH_GROUPS = 'FETCH_GROUPS';
export const FETCH_GROUP = 'FETCH_GROUP';
export const CREATE_GROUP = 'CREATE_GROUP';
export const DELETE_GROUP = 'DELETE_GROUP';

export const FETCH_USERS = 'FETCH_USERS';
export const FETCH_USER = 'FETCH_USER';
export const CREATE_USER = 'CREATE_USER';
export const DELETE_USER = 'DELETE_USER';

const API_URL = 'http://127.0.0.1:3000/api';

export function fetchGroups() {
  const request = axios.get(`${API_URL}/groups`);

  return {
    type: FETCH_GROUPS,
    payload: request
  };
}

export function fetchGroup(id) {
  const request = axios.get(`${API_URL}/groups/${id}`);

  return {
    type: FETCH_GROUP,
    payload: request
  };
}

export function createGroup(values, callback) {
  const request = axios.post(`${API_URL}/groups`, values)
    .then(() => callback());

  return {
    type: CREATE_GROUP,
    payload: request
  };
}

export function deleteGroup(id, callback) {
  const request = axios.delete(`${API_URL}/groups/${id}`)
    .then(() => callback());;

  return{
    type: DELETE_GROUP,
    payload: id
  }
}



export function fetchUsers() {
  const request = axios.get(`${API_URL}/users`);

  return {
    type: FETCH_USERS,
    payload: request
  };
}

export function fetchUser(id) {
  const request = axios.get(`${API_URL}/users/${id}`);

  return {
    type: FETCH_USER,
    payload: request
  };
}

export function createUser(values, callback) {
  const request = axios.post(`${API_URL}/users`, values)
    .then(() => callback());

  return {
    type: CREATE_USER,
    payload: request
  };
}

export function deleteUser(id, callback) {
  const request = axios.delete(`${API_URL}/users/${id}`)
    .then(() => callback());;

  return{
    type: DELETE_USER,
    payload: id
  }
}
