import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom';
import promise from 'redux-promise';

import reducers from './reducers';
import GroupsIndex from './containers/groups_index';
import GroupsNew from './containers/groups_new';
import GroupsShow from './containers/groups_show';
import UsersIndex from './containers/users_index';
import UsersNew from './containers/users_new';
import UsersShow from './containers/users_show';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
      <div>
        <div className="navbar navbar-default">
          <ul className="nav navbar-nav">
            <li><Link to="/" activeClassName="active">Groups</Link></li>
            <li><Link to="/users" activeClassName="active">Users</Link></li>
          </ul>
        </div>
        <Switch>
          <Route path="/groups/new" component={GroupsNew} />
          <Route path="/groups/:id" component={GroupsShow} />
          <Route path="/users/new" component={UsersNew} />
          <Route path="/users/:id" component={UsersShow} />
          <Route path="/users" component={UsersIndex} />
          <Route path="/" component={GroupsIndex} />
        </Switch>
      </div>
    </BrowserRouter>
  </Provider>
  , document.querySelector('.container'));
