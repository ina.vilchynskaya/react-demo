import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class UsersTable extends Component {

  renderUsers() {
    return this.props.users.map((user, index) => {
      return (
        <tr key={ user._id }>
          <td>{ index + 1 }</td>
          <td>{ user.first_name }</td>
          <td>{ user.last_name }</td>
          <td>{ user.email }</td>
          <td>{ user.gender === "M" ? "Male" : "Female" }</td>
          <td><Link className="pull-xs-right btn btn-xs btn-primary" to={"/users/" + user._id}>View</Link></td>
        </tr>
      );
    })
  }

  render() {
    return (
      <table className="table table-striped table-hover ">
        <thead>
          <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Gender</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {this.renderUsers()}
        </tbody>
      </table>
    );
  }
}

export default UsersTable;
