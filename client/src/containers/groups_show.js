import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchGroup, deleteGroup } from '../actions/index';
import { Link } from 'react-router-dom';
import Confirm from 'react-confirm-bootstrap';

import UsersTable from '../components/users_table';

class GroupsShow extends Component {
  componentWillMount() {
    this.props.fetchGroup(this.props.match.params.id);
  }

  onDeleteClick() {
    this.props.deleteGroup(this.props.match.params.id, () => {
      this.props.history.push('');
    });
  }

  render() {
    const { group } = this.props;
    if(!group) {
      return <div>Loading...</div>;
    }
    const deleteBody = "Are you sure you want to delete this Group? " + (group.users.length>0 ? 'This group has ' + group.users.length + ' users' : '');
    return (
      <div>
        <Link to={"/"}>Back to Groups</Link>
        <Confirm
            onConfirm={ this.onDeleteClick.bind(this) }
            body={ deleteBody }
            confirmText="Confirm Delete"
            title="Deleting">
            <button className="pull-xs-right btn btn-sm btn-danger">Delete Group</button>
        </Confirm>
        <h2>{ group.title }</h2>
        <p>{ group.description }</p>
        <ul className="nav nav-pills">
          <li><a href="">Users ({ group.users.length })</a></li>
        </ul>
        <UsersTable users={ group.users }/>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return { group: state.groups.group };
}

export default connect(mapStateToProps, { fetchGroup, deleteGroup })(GroupsShow);
