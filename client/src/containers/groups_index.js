import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchGroups } from '../actions/index';
import { Link } from 'react-router-dom';

class GroupsIndex extends Component {
  componentWillMount() {
    this.props.fetchGroups();
  }

  renderGroups() {
    return this.props.groups.map((group) => {
      return (
        <li className="list-group-item" key={group._id}>
          <Link className="pull-xs-right btn btn-xs btn-primary" to={"/groups/" + group._id}>View</Link>
          {group.title}
          <span className="label label-info">{group.users.length} users</span>
        </li>
      );
    })
  }

  render() {
    const { groups } = this.props;
    if(!groups) {
      return <div>Loading...</div>;
    }
    return (
      <div>
        <div className="pull-xs-right">
          <Link className="btn btn-sm btn-success" to={"/groups/new"}>Create New Group</Link>
        </div>
        <h3>Groups</h3>
        <ul className="list-group">{this.renderGroups()}</ul>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return { groups: state.groups.all };
}

export default connect(mapStateToProps, { fetchGroups })(GroupsIndex);
