import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createUser } from '../actions';

class UsersNew extends Component {

  renderInput(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? 'has-danger' : ''}`
    return(
      <div className={className}>
        <label className="control-label">{field.label}</label>
        <div>
          <input
            className="form-control"
            type={field.type}
            {...field.input}
           />
           <span className="text-help">{touched ? error : ''}</span>
        </div>
      </div>
    )
  };

  renderRadio() {
    return(
      <div className='form-group'>
        <label className="control-label">Gender</label>
        <div className="radio">
          <label>
            <Field
              name="gender"
              component="input"
              type="radio"
              value="M"
            />
            Male
          </label>
          <label>
            <Field
              name="gender"
              component="input"
              type="radio"
              value="F"
            />
            Female
          </label>
        </div>
      </div>
    )
  };

  onSubmit(values) {
    this.props.createUser(values, () => {
      this.props.history.push('');
    });

  }

  render(){
    const { handleSubmit } = this.props;
    return (
      <div className="col-lg-6 col-lg-offset-3">
        <form onSubmit={handleSubmit(this.onSubmit.bind(this))} className="bs-component">
          <Field
            label="First Name *"
            name="first_name"
            type="text"
            component={this.renderInput}
          />
          <Field
            label="Last Name *"
            name="last_name"
            type="text"
            component={this.renderInput}
          />
          <Field
            label="Email *"
            name="email"
            type="text"
            component={this.renderInput}
          />
          {this.renderRadio()}
          <button type="submit" className="btn btn-primary">Add Group</button>
          <Link to="/" className="btn btn-danger">Cancel</Link>
        </form>
      </div>
    )
  }
}

const validate = values => {
  const errors = {};
  console.log('values', values)

  if (!values.first_name) {
    errors.first_name = 'First Name is required';
  }

  if (!values.last_name) {
    errors.last_name = 'Last Name is required';
  }

  if (!values.email) {
    errors.email = 'Email is required';
  }

  return errors;
}

export default reduxForm({
  form: 'NewUserForm',
  validate
})(
  connect(null, { createUser })(UsersNew)
);
