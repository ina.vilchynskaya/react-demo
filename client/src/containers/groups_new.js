import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createGroup } from '../actions';

class GroupsNew extends Component {

  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? 'has-danger' : ''}`
    return(
      <div className={className}>
        <label className="control-label">{field.label}</label>
        <div>
          <input
            className="form-control"
            type="text"
            {...field.input}
           />
           <span className="text-help">{touched ? error : ''}</span>
        </div>
      </div>
    )
  };

  onSubmit(values) {
    this.props.createGroup(values, () => {
      this.props.history.push('');
    });

  }

  render(){
    const { handleSubmit } = this.props;
    return (
      <div className="col-lg-6 col-lg-offset-3">
        <form onSubmit={handleSubmit(this.onSubmit.bind(this))} className="bs-component">
          <Field
            label="Title"
            name="title"
            component={this.renderField}
          />
          <Field
            label="Description"
            name="description"
            component={this.renderField}
          />
          <button type="submit" className="btn btn-primary">Add Group</button>
          <Link to="/" className="btn btn-danger">Cancel</Link>
        </form>
      </div>
    )
  }
}

const validate = values => {
  const errors = {};

  if (!values.title) {
    errors.title = 'Title is required';
  }
  if (!values.description) {
    errors.description = 'Description is required';
  }
  return errors;
}

export default reduxForm({
  form: 'NewGroupForm',
  validate
})(
  connect(null, { createGroup })(GroupsNew)
);
