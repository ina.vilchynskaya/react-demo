import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchUsers } from '../actions/index';
import { Link } from 'react-router-dom';

import UsersTable from '../components/users_table';

class UsersIndex extends Component {
  componentWillMount() {
    this.props.fetchUsers();
  }

  renderUsers() {
    return this.props.users.map((user, index) => {
      return (
        <tr key={user._id}>
          <td>{ index + 1 }</td>
          <td>{user.first_name}</td>
          <td>{user.last_name}</td>
          <td>{user.email}</td>
          <td>{user.gender}</td>
          <td><Link className="pull-xs-right btn btn-xs btn-primary" to={"/users/" + user._id}>View</Link></td>
        </tr>
      );
    })
  }

  render() {
    const { users } = this.props;
    if(!users) {
      return <div>Loading...</div>;
    }
    return (
      <div>
        <div className="pull-xs-right">
          <Link className="btn btn-sm btn-success" to={"/groups/new"}>Create New Group</Link>
        </div>
        <h3>Users</h3>
        <UsersTable users={ users }/>
      </div>
    )
  }

}

function mapStateToProps(state) {
  return { users: state.users.all };
}

export default connect(mapStateToProps, { fetchUsers })(UsersIndex);
