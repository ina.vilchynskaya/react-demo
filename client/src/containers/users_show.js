import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchUser, deleteUser } from '../actions/index';
import { Link } from 'react-router-dom';

import Confirm from 'react-confirm-bootstrap';

class UsersShow extends Component {
  componentWillMount() {
    this.props.fetchUser(this.props.match.params.id);
  }

  onDeleteClick() {
    this.props.deleteUser(this.props.match.params.id, () => {
      this.props.history.push('');
    });
  }

  render() {
    const { user } = this.props;
    if(!user) {
      return <div>Loading...</div>;
    }
    return (
      <div>
        <Link to={"/"}>Back to Users</Link>
        <Confirm
            onConfirm={ this.onDeleteClick.bind(this) }
            body="Are you sure you want to delete this User?"
            confirmText="Confirm Delete"
            title="Deleting">
            <button className="pull-xs-right btn btn-sm btn-danger">Delete User</button>
        </Confirm>
        <h2>{ user.first_name } { user.last_name }</h2>
        <div>{ user.gender=="M"?"Male":"Female" }</div>
        <b>{ user.email }</b>
        <br/><br/>
        <div>Groups: </div>
        { user.groups.map((group) => {
          return (
            <Link to={"/groups/" + group._id} key="{ group._id }">{ group.title }</Link>
          )
        }) }
      </div>
    )
  }
}

function mapStateToProps({ users }) {
  return { user: users.user };
}

export default connect(mapStateToProps, { fetchUser, deleteUser })(UsersShow);
