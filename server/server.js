const MongoClient = require('mongodb').MongoClient;
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');


const app = express();
const hostname = '127.0.0.1';
const port = 3000;

app.use( bodyParser.json() );

let db;
MongoClient.connect('mongodb://admin:admin@ds153682.mlab.com:53682/react-demo', function(err, database) {
  if (err) return console.log(err);
  db = database;
  app.listen(port, hostname, () => {
    console.log('listening on 3000')
  })
});

app.use(function(req, res, next) {
 res.setHeader('Access-Control-Allow-Origin', '*');
 res.setHeader('Access-Control-Allow-Credentials', 'true');
 res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
 res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
 res.setHeader('Cache-Control', 'no-cache');
 next();
});

function getNextSequence(name, callback) {
  var docdoc = db.collection('counters').findAndModify(
      { _id: name },
      [],
      { $inc: { seq: 1 } },
      { new: true },
      function(err, doc) {
        callback(doc.value.seq);
      }
   );
}


//------  Groups

app.get('/api/groups', (req, res) => {
  db.collection('groups').find().toArray((err, result) => {
    if (err) return console.log(err);
    res.send(result);
  })
})

app.get('/api/groups/:id', (req, res) => {
  db.collection('groups').findOne({ "_id": parseInt(req.params.id) }, (err, result) => {
    if (err) return console.log(err);
    var userIds = result.users.map(function(v){ return v._id });
    db.collection('users').find({ _id: { $in: userIds } }).toArray((err, docs) => {
      if (err) return console.log(err);
      result.users = docs;
      res.json(result);
    });

  })

})

app.post('/api/groups', (req, res) => {
  var title = req.body.title;
  var description = req.body.description;

  getNextSequence("groupid", function(id){
    db.collection('groups').insert(
       {
         _id: id,
         title: title,
         description: description,
         users: []
       },
       function(err, result) {
          if(err) throw err;
          res.sendStatus(200);
        });
   }
  )
})

app.delete('/api/groups/:id', (req, res) => {
  db.collection('groups').deleteOne({ "_id": parseInt(req.params.id) }, (err, result) => {
    if (err) return console.log(err);
    res.send(req.params.id);
  })
})


//------  Users

app.get('/api/users', (req, res) => {
  db.collection('users').find().toArray((err, result) => {
    if (err) return console.log(err);
    res.send(result);
  })
})

app.get('/api/users/:id', (req, res) => {
  db.collection('users').findOne({ "_id": parseInt(req.params.id) }, (err, result) => {
    if (err) return console.log(err);

    db.collection('groups').find({ users: { _id: parseInt(req.params.id)} }).toArray((err, docs) => {
      if (err) return console.log(err);
      result.groups = docs;
      res.json(result);
    });

  })
})

app.post('/api/users', (req, res) => {
  var first_name = req.body.first_name;
  var last_name = req.body.last_name;
  var email = req.body.email;
  var gender = req.body.gender || '';

  getNextSequence("userid", function(id){
    db.collection('users').insert(
       {
         _id: id,
         first_name: first_name,
         last_name: last_name,
         email: email,
         gender: gender
       },
       function(err, result) {
          if(err) throw err;
          res.sendStatus(200);
        });
   }
  )
})

app.delete('/api/users/:id', (req, res) => {
  db.collection('users').deleteOne({ "_id": parseInt(req.params.id) }, (err, result) => {
    if (err) return console.log(err);
    db.collection('groups').update(
        { },
        { $pull: { users: { _id: parseInt(req.params.id)} } },
        { multi: true },
        function(err, docs){
          if (err) return console.log(err);
          res.send(req.params.id);
        })
  })
})
